package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.logging.Logger;

import deloitte.academy.lesson01.machine.Products;

/**
 * Main class of exercise Vending Machine
 * 
 * @author dmascott
 *
 */
public class RunMachine {

	/*
	 * A global Array with Products values is created
	 */
	public static final ArrayList<Products> productsList = new ArrayList<Products>();
	public static final ArrayList<Products> salesList = new ArrayList<Products>();
	public final static Logger LOGGER = Logger.getLogger(RunMachine.class.getName());

	public static void main(String[] args) {

		/*
		 * The objects products are instantiated as products
		 */
		Products prod1 = new Products("A1", "Chocolate", 10.50, 10);
		Products prod2 = new Products("A2", "Doritos", 15.50, 4);
		Products prod3 = new Products("A3", "Coca", 22.50, 2);
		Products prod4 = new Products("A4", "Gomitas", 8.75, 6);
		Products prod5 = new Products("A5", "Chips", 30.00, 10);
		Products prod6 = new Products("A6", "Jugo", 15.00, 2);
		Products prod7 = new Products("B1", "Galletas", 10.00, 3);
		Products prod8 = new Products("B2", "Canelitas", 120.00, 6);
		Products prod9 = new Products("B3", "Halls", 10.10, 10);
		Products prod10 = new Products("B4", "Tarta", 3.14, 10);
		Products prod11 = new Products("B5", "Sabritas", 15.55, 0);
		Products prod12 = new Products("B6", "Cheetos", 12.25, 4);
		Products prod13 = new Products("C1", "Rocaleta", 10.00, 1);
		Products prod14 = new Products("C2", "Rancherito", 14.75, 6);
		Products prod15 = new Products("C3", "Ruffles", 13.15, 10);

		/*
		 * The products are added to the ArrayList "productsList"
		 */
		productsList.add(prod1);
		productsList.add(prod2);
		productsList.add(prod3);
		productsList.add(prod4);
		productsList.add(prod5);
		productsList.add(prod6);
		productsList.add(prod7);
		productsList.add(prod8);
		productsList.add(prod9);
		productsList.add(prod10);
		productsList.add(prod11);
		productsList.add(prod12);
		productsList.add(prod13);
		productsList.add(prod14);
		productsList.add(prod15);

		System.out.println(prod1.getCode());

		/*
		 * An object Products is instantiated
		 */
		Products methods = new Products();

		/*
		 * The method listProds of the class Products is called to show all the products
		 * registered before running any other method
		 */
		ArrayList<String> products = methods.listProds(productsList);
		System.out.println("Products list: " + products);

		/*
		 * A new product is created
		 */
		Products prod16 = new Products("C4", "Pizza Fr�a", 22.00, 9);

		/*
		 * The method createProd is called
		 */
		System.out.println("\nCREATE");
		methods.createProd(prod16);

		/*
		 * The method listProds of the class Products is called to show all the products
		 * registered after creating a new product
		 */
		ArrayList<String> productsCreate = methods.listProds(productsList);
		System.out.println("\nProducts list: " + productsCreate);

		/*
		 * The method readProd is called
		 */
		System.out.println("\nREAD");
		methods.readProd("A1");

		/*
		 * The method updateProd is called
		 */
		System.out.println("\nUPDATE");
		methods.updateProd("C3", "Oreos", 10.00, 12);

		/*
		 * The method listProds of the class Products is called to show all the products
		 * registered after updating a product
		 */
		ArrayList<String> productsUpdate = methods.listProds(productsList);
		System.out.println("\nProducts list: " + productsUpdate);

		/*
		 * The method deleteByCode is called
		 */
		System.out.println("\nDELETE");
		methods.deleteByCode("C3");

		/*
		 * The method listProds of the class Products is called to show all the products
		 * registered after deleting a product by the method deleteByCode
		 */
		ArrayList<String> productsDelete1 = methods.listProds(productsList);
		System.out.println("\nProducts list: " + productsDelete1);

		/*
		 * The method deleteByName is called
		 */
		methods.deleteByName("Pepsi");

		/*
		 * The method listProds of the class Products is called to show all the products
		 * registered after deleting a product by the method deleteByName
		 */
		ArrayList<String> productsDelete2 = methods.listProds(productsList);
		System.out.println("\nProducts list: " + productsDelete2);

		/*
		 * Two String variables are created with different dates
		 */
		String today = "03/10";
		String tomorrow = "04/10";

		/*
		 * The objects products are instantiated as sales
		 */
		Products sales1 = new Products("D5", today);
		Products sales2 = new Products("B5", today);
		Products sales3 = new Products("A5", tomorrow);
		Products sales4 = new Products("A3", tomorrow);
		Products sales5 = new Products("A6", tomorrow);

		/*
		 * The method listSales of the class Products is called to show all the sales
		 * registered before running any other method
		 */
		System.out.println("\nSALES");
		ArrayList<String> salesL = methods.listSales(salesList);
		System.out.println("Sales list: " + salesL);

		/*
		 * The method sales is called
		 */
		methods.sales(sales1);
		methods.sales(sales2);

		/*
		 * The method listSales of the class Products is called to show all the sales
		 * registered after the sales of the first day
		 */
		ArrayList<String> salesL1 = methods.listSales(salesList);
		System.out.println("\nSales list: " + salesL1);

		/*
		 * The method sales is called
		 */
		methods.sales(sales3);
		methods.sales(sales4);
		methods.sales(sales5);

		/*
		 * The method listSales of the class Products is called to show all the sales
		 * registered after the sales of the second day
		 */
		ArrayList<String> salesL2 = methods.listSales(salesList);
		System.out.println("\nSales list: " + salesL2);

		/*
		 * The method totalSales is called
		 */
		System.out.println("\nTOTAL SALES");
		System.out.println("The total sales is: $" + methods.totalSales());

		/*
		 * The method salesByDay is called
		 */
		System.out.println("\nSALES BY DAY");
		ArrayList<String> salesDay = methods.salesByDay(today);
		System.out.println("Sales on day: " + today + salesDay);

	}

}
