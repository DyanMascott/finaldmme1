package deloitte.academy.lesson01.entity;

/**Enum Messages
 * 
 * @author dmascott
 */
public enum Messages {
	/*
	 * The Definition of the different types of messages
	 */
	Existing("The code registered already exists"), Registered("The product has been registered successfully"),
	NotFound("Product not found"), Updated("The product has been updated successfully"),
	Deleted("The product has been deleted successfully"), Sold("Thanks for your purchase"),
	SoldOut("The product is sold out, please introduce another code"), NoSalesDay("There were no sales that day"),
	WrongQuantity("The introduced quantity is not higher than 0, please change it");

	/*
	 * Constructor of the enum
	 */
	private Messages(String message) {
		this.message = message;
	}

	/*
	 * A property message to show
	 */
	private String message;

	/*
	 * Getter and setter of the property message from Messages
	 */
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
