package deloitte.academy.lesson01.entity;

import java.util.ArrayList;

import deloitte.academy.lesson01.machine.Products;

/**
 * Abstract class VendingMachine
 * 
 * @author dmascott
 *
 */
public abstract class VendingMachine {

	/*
	 * The variables are created
	 */
	private String code;
	private String nameProd;
	private double price;
	private int quantity;
	private String date;

	/**
	 * Superclass Constructor
	 */
	public VendingMachine() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * The constructor of the object Products is created
	 * 
	 * @param code     String value
	 * @param nameProd String value
	 * @param price    double value
	 * @param quantity int value
	 * @param date     String value
	 */
	public VendingMachine(String code, String nameProd, double price, int quantity, String date) {
		super();
		this.code = code;
		this.nameProd = nameProd;
		this.price = price;
		this.quantity = quantity;
		this.date = date;
	}

	public VendingMachine(String code, String nameProd, double price, int quantity) {
		super();
		this.code = code;
		this.nameProd = nameProd;
		this.price = price;
		this.quantity = quantity;
	}

	public VendingMachine(String code, String date) {
		super();
		this.code = code;
		this.date = date;
	}

	/*
	 * Getters and Setters are created
	 */
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNameProd() {
		return nameProd;
	}

	public void setNameProd(String nameProd) {
		this.nameProd = nameProd;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @param productList Array with object Products values
	 * @return Array of String values
	 * @throws General Exception
	 */
	public abstract ArrayList<String> listProds(ArrayList<Products> productList);

	/**
	 * 
	 * @param product Object Product
	 * @throws General Exception
	 */
	public abstract void createProd(Products product);

	/**
	 * 
	 * @param code String value
	 * @throws General Exception
	 */
	public abstract void readProd(String code);

	/**
	 * 
	 * @param code     String value
	 * @param name     String value
	 * @param price    double value
	 * @param quantity int value
	 * @return Object Products
	 * @throws General Exception
	 */
	public abstract Products updateProd(String code, String name, double price, int quantity);

	/**
	 * 
	 * @param code String value
	 * @throws General Exception
	 */
	public abstract void deleteByCode(String code);

	/**
	 * 
	 * @param name String value
	 * @throws General Exception
	 */
	public abstract void deleteByName(String name);

	/**
	 * 
	 * @param sale Object Products
	 * @return Object Products
	 * @throws General Exception
	 */
	public abstract Products sales(Products sale);

	/**
	 * 
	 * @param saleList Array with Objects Products values
	 * @return Array with String values
	 * @throws General Exception
	 */
	public abstract ArrayList<String> listSales(ArrayList<Products> saleList);

	/**
	 * 
	 * @return double
	 * @throws General Exception
	 */
	public abstract double totalSales();

	/**
	 * 
	 * @param date String value
	 * @return ArrayList with String values
	 * @throws General Exception
	 */
	public abstract ArrayList<String> salesByDay(String date);
}
