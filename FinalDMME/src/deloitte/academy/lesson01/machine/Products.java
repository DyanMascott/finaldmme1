package deloitte.academy.lesson01.machine;

import java.util.ArrayList;
import java.util.logging.Level;

import deloitte.academy.lesson01.entity.Messages;
import deloitte.academy.lesson01.entity.VendingMachine;
import deloitte.academy.lesson01.run.RunMachine;

/**
 * Class Products
 * 
 * @author dmascott
 *
 */
public class Products extends VendingMachine {

	/**
	 * Superclass constructor
	 */
	public Products() {
		super();
	}

	/**
	 * A constructor of the object Products is created with the values:
	 * 
	 * @param code     String value
	 * @param nameProd String value
	 * @param price    double value
	 * @param quantity int value
	 * @param date     String value
	 */
	public Products(String code, String nameProd, double price, int quantity, String date) {
		super(code, nameProd, price, quantity, date);
	}

	/**
	 * A constructor of the object Products is created with the values:
	 * 
	 * @param code     String value
	 * @param nameProd String value
	 * @param price    double value
	 * @param quantity int value
	 */
	public Products(String code, String nameProd, double price, int quantity) {
		super(code, nameProd, price, quantity);
	}

	/**
	 * A constructor of the object Products is created with the values:
	 * 
	 * @param code String value
	 * @param date String value
	 */
	public Products(String code, String date) {
		super(code, date);
	}

	/**
	 * Calls to the abstract method listProds
	 * 
	 * Query of all the products
	 */
	@Override
	public ArrayList<String> listProds(ArrayList<Products> productList) {
		ArrayList<String> product = new ArrayList<String>();
		try {
			for (Products prod : productList) {
				product.add("\nCode : " + prod.getCode() + "\t");
				product.add("Product: " + prod.getNameProd() + "\t");
				product.add("Price: " + prod.getPrice() + "\t");
				product.add("Quantity: " + prod.getQuantity() + "");
			}

		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return product;
	}

	/**
	 * Calls to the abstract method createProd
	 * 
	 * Checks if the product already exist, if not the product its created, added to
	 * the productsList and a message of success is sent, otherwise a message of
	 * error is sent
	 * 
	 */
	@Override
	public void createProd(Products product) {
		try {
			int val = 0;
			for (Products prod : RunMachine.productsList) {
				if (prod.getCode() == product.getCode() || prod.getNameProd() == product.getNameProd()) {
					RunMachine.LOGGER.info(Messages.Existing.getMessage());
					val = 1;
				}
			}
			if (val != 1) {
				RunMachine.productsList.add(product);
				RunMachine.LOGGER.info(Messages.Registered.getMessage() + ": " + product.getNameProd());
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
	}

	/**
	 * Calls to the abstract method readProd
	 * 
	 * Checks if any of the products in the productsList has the same code as the
	 * code sent in the parameters, if the code is found then the name of the
	 * product containing that code is shown in the console, otherwise a not found
	 * error is sent
	 */
	@Override
	public void readProd(String code) {
		try {
			int val = 0;
			for (Products prod : RunMachine.productsList) {
				if (prod.getCode() == code) {
					RunMachine.LOGGER.info("The product with the code " + code + " is " + prod.getNameProd());
					val = 1;
					break;
				}
			}
			if (val != 1) {
				RunMachine.LOGGER.info(Messages.NotFound.getMessage());
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
	}

	/**
	 * Calls to the abstract method updateProd
	 * 
	 * Checks if any of the products in the productsList has the same code as the
	 * code sent in the parameters, if the code is found then the name, the price
	 * and the quantity of the product is replaced by the values given in the
	 * parameters and a message of success is sent, otherwise a not found error is
	 * sent
	 */
	@Override
	public Products updateProd(String code, String name, double price, int quantity) {
		Products product = new Products();
		try {
			int val = 0;
			for (Products prod : RunMachine.productsList) {
				if (prod.getCode() == code) {
					if (quantity >= 0) {
						prod.setNameProd(name);
						prod.setPrice(price);
						prod.setQuantity(quantity);
						val = 1;
						product = prod;
						RunMachine.LOGGER.info(Messages.Updated.getMessage());
						break;
					} else {
						RunMachine.LOGGER.info(Messages.WrongQuantity.getMessage());
						val = 1;
						break;
					}
				}
			}
			if (val != 1) {
				RunMachine.LOGGER.info(Messages.NotFound.getMessage());
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return product;
	}

	/**
	 * Calls to the abstract method deleteByCode
	 * 
	 * Checks if any of the products in the productsList has the same code as the
	 * code sent in the parameters, if the code is found then the product that
	 * contains that code is deleted from the productsList and a message of success
	 * is sent, otherwise a not found error is sent
	 */
	@Override
	public void deleteByCode(String code) {
		try {
			int val = 0;
			for (Products prod : RunMachine.productsList) {
				if (prod.getCode() == code) {
					val = 1;
					RunMachine.productsList.remove(prod);
					RunMachine.LOGGER.info(Messages.Deleted.getMessage());
					break;
				}
			}
			if (val != 1) {
				RunMachine.LOGGER.info(Messages.NotFound.getMessage());
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
	}

	/**
	 * Calls to the abstract method deleteByName
	 * 
	 * Checks if any of the products in the productsList has the same name as the
	 * name sent in the parameters, if the name is found then the product that
	 * contains that name is deleted from the productsList and a message of success
	 * is sent, otherwise a not found error is sent
	 */
	@Override
	public void deleteByName(String name) {
		try {
			int val = 0;
			for (Products prod : RunMachine.productsList) {
				if (prod.getNameProd() == name) {
					val = 1;
					RunMachine.productsList.remove(prod);
					RunMachine.LOGGER.info(Messages.Deleted.getMessage());
					break;
				}
			}
			if (val != 1) {
				RunMachine.LOGGER.info(Messages.NotFound.getMessage());
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
	}

	/**
	 * Calls to the abstract method sales
	 * 
	 * Checks if the code of the sale sent in the parameters matches with the code
	 * of the products of the productsList, if it matches and the quantity of the
	 * product is higher than 0, then the sale is added to salesList, if the
	 * quantity of the product is 0 then a soldOut message is sent, if the values
	 * doesn't match, a not found error message is sent
	 */
	@Override
	public Products sales(Products sale) {
		Products newSale = new Products();
		try {
			int val = 0;
			for (Products prod : RunMachine.productsList) {
				if (prod.getCode() == sale.getCode()) {
					if (prod.getQuantity() != 0 || sale.getQuantity() != 0) {
						prod.setQuantity(prod.getQuantity() - 1);
						sale.setDate(sale.getDate());
						sale.setCode(prod.getCode());
						sale.setQuantity(prod.getQuantity() - 1);
						prod.setQuantity(prod.getQuantity() - 1);
						sale.setNameProd(prod.getNameProd());
						sale.setPrice(prod.getPrice());
						newSale = sale;
						RunMachine.salesList.add(newSale);
						RunMachine.LOGGER.info(Messages.Sold.getMessage());
						val = 1;
						break;
					} else {
						RunMachine.LOGGER.info(Messages.SoldOut.getMessage());
						val = 1;
						break;
					}
				}
			}
			if (val != 1) {
				RunMachine.LOGGER.info(Messages.NotFound.getMessage());
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return newSale;
	}

	/**
	 * Calls to the abstract method listSales
	 * 
	 * Query of all the sales
	 */
	@Override
	public ArrayList<String> listSales(ArrayList<Products> saleList) {
		ArrayList<String> sale = new ArrayList<String>();
		try {
			for (Products sales : saleList) {
				sale.add("\nCode : " + sales.getCode() + "\t");
				for (Products prod : RunMachine.productsList) {
					if (prod.getCode() == sales.getCode()) {
						sale.add("Product: " + sales.getNameProd() + "\t");
						sale.add("Quantity: " + sales.getQuantity() + "\t");
						break;
					}
				}
				sale.add("Date: " + sales.getDate() + "\t");
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return sale;
	}

	/**
	 * Calls to the abstract method totalSales
	 * 
	 * Checks the products in the salesList and add the price of each product to the
	 * variable total
	 */
	@Override
	public double totalSales() {
		double total = 0;
		try {
			for (Products sales : RunMachine.salesList) {
				for (Products prod : RunMachine.productsList) {
					if (prod.getCode() == sales.getCode()) {
						total = total + prod.getPrice();
						break;
					}
				}
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return total;
	}

	/**
	 * Calls to the abstract method salesByDay
	 * 
	 * Checks in the salesList if any product matches the date sent in the
	 * parameters, if matches then saves in an ArrayList the values of every product
	 * that was sold on that date, otherwise it'll send No Sales that Day message<<
	 */
	@Override
	public ArrayList<String> salesByDay(String date) {
		ArrayList<String> salesList1 = new ArrayList<String>();
		try {
			for (Products sale : RunMachine.salesList) {
				if (sale.getDate() == date) {
					salesList1.add("\nCode: " + sale.getCode() + "\t");
					for (Products prod : RunMachine.productsList) {
						if (prod.getCode() == sale.getCode()) {
							salesList1.add("Product: " + sale.getNameProd());
						}
					}
					break;
				} else {
					RunMachine.LOGGER.info(Messages.NoSalesDay.getMessage());
					break;
				}
			}
		} catch (Exception e) {
			RunMachine.LOGGER.log(Level.SEVERE, "The exception is: ", e);
		}
		return salesList1;
	}
}